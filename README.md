[![made-with-pythone](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
# Simple Flight Project

A simple project for the relation of FLights, Airports and
Passengers with Python, Django framework which includes the 
following:

- One to Many PostgreSQL database.
- A few customized filter for Admin.
- Back-End and Client testing.
- Travis.yml for CI.
- Docker files to create an image.




